class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
    	t.integer :object_type_id
    	t.string :object_type , :default => "post"
    	t.integer :user_id
    	t.integer :parent_comment_id
    	t.text :comment
    	t.boolean :deleted, :default => false
      t.timestamps
    end
  end
end
