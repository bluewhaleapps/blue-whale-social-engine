class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
    	t.integer :user_id
    	t.integer :object_type_id
    	t.string :object_type
      t.timestamps
    end
  end
end
