class CreatePosts < ActiveRecord::Migration
  def change
	  create_table "posts", :force => true do |t|
	    t.integer  "parent_post_id"
	    t.integer  "user_id"
	    t.text     "post_text"
	    t.boolean  "public",         :default => true
	    t.boolean  "is_deleted",        :default => false
	    t.datetime "created_at",                        :null => false
	    t.datetime "updated_at",                        :null => false
	    t.string   "location"
	    t.string   "zip"
	    t.string   "flag"
	  end
  end
end
