class ChangeLikesObjectIdCol < ActiveRecord::Migration
  def change
  	rename_column :likes, :object_id, :object_type_id
  end
end
