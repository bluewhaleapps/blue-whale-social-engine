class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
    	t.integer :user_id
    	t.string :invited_email
    	t.string :invited_id
      t.timestamps
    end
  end
end
