class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
    	t.string :name
    	t.text :description
    	t.integer :object_owner_id
    	t.string :object_type => "User"
      t.timestamps
    end
  end
end
