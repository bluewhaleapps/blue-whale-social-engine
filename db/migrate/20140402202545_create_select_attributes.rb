class CreateSelectAttributes < ActiveRecord::Migration
  def change
    create_table :select_attributes do |t|
      t.string :name
      t.hstore :options
      t.timestamps
    end

    add_index :select_attributes, :name, :unique => true
  end
end
