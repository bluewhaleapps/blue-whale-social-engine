class CreatePostSubscribers < ActiveRecord::Migration
  def change
    create_table :post_subscribers do |t|
    	t.integer :post_id
    	t.integer :user_id
    	t.integer :group_id
    	t.boolean :blocked, :default => false
    end
  end
end
