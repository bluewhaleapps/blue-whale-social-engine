BluewhaleSocial::Engine.routes.draw do 
 	
  get "select_attributes/get_options_by_name"
	get "/reset_password/:reset_token" => "sessions#reset_password"
	put "/change_password/:reset_token" => "sessions#change_password"
	 
		post "/login" => "sessions#create"
		put "/forgot_username" => "sessions#forgot_username"
		put "/forgot_password" => "sessions#forgot_password"
		resources :sessions,:register

		get '/users/facebook_friends' => "users#facebook_friends"
		get '/users/search_emails' => "users#search_emails"
		post '/users/invite' => "users#invite" 
		get '/users/categories' => "users#categories"
		delete '/users/destroy_category/:id' => "users#destroy_category"
		resources :users do
			get :likes
			get :favorites
			get :user_likes
			post 'invite_decline'
			post 'invite_accept'
            post 'change_photo'
            delete :kill
			post :favorite
			delete :unfavorite
			post :like
			delete :unlike
			get :invites
		end
		resources :user_groups,:post_comments,:categories
		
		resources :followers do 
			get :followings
			get :pending
		end

		get "/posts/popular" => "posts#popular"
		get "/posts/recent" => "posts#recent"
		get "/posts/top" => "posts#top"
		get '/posts/categories' => "posts#categories"
		delete '/posts/destroy_category/:id' => "posts#destroy_category"
		resources :posts do
			get :user_likes
			delete :kill
			post :like
			delete :unlike
			post :favorite
			delete :unfavorite
			post :flag
    end

    get "get_options_by_name/:name" => "select_attributes#get_options_by_name"
 	
 	#get "*blahblah" => "api#routing_error"
end
