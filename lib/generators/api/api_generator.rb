class ApiGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  @@source_root = File.expand_path('../templates', __FILE__)

  def copy_config_files
  	api_key = Digest::SHA256.hexdigest("#{Time.now}#{SecureRandom.hex}")
  	api_config_file = YAML.load_file("#{@@source_root}/api.yml")
  	api_config_file["development"]["api_key"] = api_key
  	api_config_file["test"]["api_key"] = api_key
  	api_config_file["production"]["api_key"] = api_key
    create_file "config/api.yml",api_config_file.to_yaml

    copy_file "s3.yml", "config/s3.yml"

    api_route = 'mount BluewhaleSocial::Engine => "/api"'
    route api_route

  end
  
end
