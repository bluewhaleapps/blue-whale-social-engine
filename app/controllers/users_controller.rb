class UsersController < ApiController
	before_filter :validate_auth_token

	def index
		params[:page] ||= 1
		params[:per] ||= 25
		cond = params[:cond].present? ? meta_data_condparams[:cond] : ["1=1"]
		search = params[:search].present? ? ["lower(username) like :search or lower(first_name) like :search or lower(last_name) like :search or lower(email) like :search",{:search => "#{params[:search].downcase}%"}] : []
		@users = User.where(cond).where(search).page(params[:page]).per(params[:per])
	end

	def update
		@user = current_user
		if @user.update_attributes(user_params)
			if params[:user][:categories].present?
				handle_api_error(@user) if !@user.associate_categories(params[:user][:categories])
			end

			render :update
		else
			return handle_api_error(@user)
		end
	end
    
    def change_photo
        update
    end

	def show
		@user = User.find(params[:id])
	end

	def destroy
		@user = User.find(params[:id])
		@user.update_attribute("is_deleted",true)
		render :json => {:result => "User deleted"}
	end

	def kill
		raise "Cannot kill a user in prodction" if !Rails.env.development?
		@user = User.unscoped.find(params[:user_id])
		@user.destroy if @user.present?
		render :json => {:result => "User killed"}
	end

	##
	# Pull a users facebook friends that are an active user of the app.
	def facebook_friends
		@users = []
		@graph = Koala::Facebook::API.new(current_user.oauth_token)
		friends = @graph.get_connections("me", "friends")
		if !friends.blank?
			@users = friends.map{|f| 
				existing_user = User.where(["facebook_id = ?" ,f["id"]]).first
				f["is_member"] = existing_user.present?
				f["is_following"] = false
				if existing_user.present?
					f["is_following"] = Follower.exists?(["user_id = ? and following_id = ?",current_user.id,existing_user.id])
					f["is_invited"] = Invite.exists?(["user_id = ? and invited_id = ?",current_user.id,existing_user.facebook_id])
				end
				
				{:user => f}
			}
		end
	end

	def search_emails
		@users = User.where(:email => params[:emails])
		render :index
	end

	def invite
		@invite = Invite.new(user_params)
		@invite.user_id = current_user.id
		if @invite.valid?
			
			existing_user = User.where(["facebook_id = ? or lower(email) = lower(?)" ,user_params[:invited_id],user_params[:invited_email]]).first
			if existing_user.present?
				@invite.invited_user_id = existing_user.id
			end
			@invite.save
			render :invite
		else
			return handle_api_error(@invite)
		end
	end

	def invites
		@invites = Invite.where(:user_id => current_user.id)
		render :invites
	end

	def invite_accept
		@invite = Invite.where(:invited_user_id => current_user.id, :user_id => params[:user_id]).first
		if @invite.present?
      		Follower.create(:user_id => current_user.id,:following_id => params[:user_id])
      		Follower.create(:user_id => params[:user_id],:following_id => current_user.id)
      		@pendings = current_user.pending_invites
      		render :template => "followers/pending"
		else
			return handle_api_error("In-valid invitation")
		end
	end

	def invite_decline
		@invite = Invite.where(:invited_user_id => current_user.id, :user_id => params[:user_id]).first
		if @invite.present?
			@invite.destroy
			@pendings = current_user.pending_invites
      		render :template => "followers/pending"
		else
			return handle_api_error("In-valid invitation")
		end
	end

	def categories
		@categories = Category.where(:object_type => "user").order(:name)
		render :json => {:user_categories => @categories.map{|c| c.name}   }
	end

	def destroy_category
		c = Category.where(:name => params[:id],:object_type => "user").first
		if c.present?
			c.destroy
			render :json => {:result => "Category deleted"}
		else
			handle_api_error("Category #{params[:id]} not found")
		end
	end

	##
	# Pulls a list of users that like a user
	def user_likes
		params[:page] ||= 1
		params[:per] ||= 25 
		likes  = Like.where(:object_type_id => params[:user_id],:object_type => "user").map{|u| u.user_id}
 		@users = User.where(:id => likes).page(params[:page]).per(params[:per])
		render :template => "/users/index"
	end


	##
	# Pulls a list of users that have been liked by a user
	def likes
		params[:page] ||= 1
		params[:per] ||= 25 
		likes  = Like.where(:user_id => params[:user_id],:object_type => params[:object_type]).map{|u| u.object_type_id}
		case params[:object_type]
		when 'user'
			@users = User.where(:id => likes).page(params[:page]).per(params[:per])
		when 'post'
			@posts = Post.where(:id => likes).page(params[:page]).per(params[:per])
		end
		render :template => "/#{params[:object_type].pluralize}/index"
	end


	def favorites
		params[:page] ||= 1
		params[:per] ||= 25 
		favs  = Favorite.where(:user_id => params[:user_id],:object_type => params[:object_type]).map{|u| u.object_type_id}
		case params[:object_type]
		when 'user'
			@users = User.where(:id => likes).page(params[:page]).per(params[:per])
		when 'post'
			@posts = Post.where(:id => likes).page(params[:page]).per(params[:per])
		end
	 
	end


	def like
		@user = User.find(params[:user_id])
		Like.create({:user_id => current_user.id , :object_type_id => @user.id,:object_type => "user"})
		render :show
	end

	def unlike
		@user = User.find(params[:user_id])
		Like.where(:object_type_id => @user.id, :object_type => "user", :user_id => current_user.id).first.destroy
		render :json => {:result => "Like deleted"}
	end

	def favorite
		@user = User.find(params[:user_id])
		Favorite.create({:user_id => current_user.id , :object_type_id => @user.id,:object_type => "user"})
		render :show
	end

	def unfavorite
		@user = User.find(params[:user_id])
		Favorite.where(:object_type_id => @user.id, :object_type => "user", :user_id => current_user.id).first.destroy
		render :json => {:result => "Favorite deleted"}
	end

	private
    def user_params
      params.require(:user).permit(:username, :email, :first_name, :last_name,:avatar,:company_name,:is_business,:birthdate,
      	:address1,:address2,:city, :state,:zip, :phone,:country,:invited_email,:invited_id,:categories,:oauth_token,roles_assignment:[],meta_data: params[:user][:meta_data].try(:keys)
      	) 
    end
end
