 
class PostsController < ApiController
	before_filter :validate_auth_token

	def popular
		params[:page] ||= 1
		params[:per] ||= 25
		params[:sort] ||= "posts.views desc"
		@posts = Post.order("posts.views desc,#{params[:sort]}").page(params[:page]).per(params[:per])
		render :index
	end

	def recent
		params[:page] ||= 1
		params[:per] ||= 25
		params[:sort] ||= "posts.created_at desc"
		@posts = Post.order("posts.created_at desc,#{params[:sort]}").page(params[:page]).per(params[:per])
		render :index
	end

	def top
		params[:page] ||= 1
		params[:sort] ||= "l.likes_count desc"
		@posts = Post.joins("inner join (select count(*) as likes_count,likes.object_type_id as post_id from likes where likes.object_type = 'post' group by likes.object_type_id) l on l.post_id = posts.id").order(params[:sort]).page(params[:page]).per(100)
		render :index
	end



	def index
		params[:page] ||= 1
		params[:per] ||= 25
		params[:sort] ||= "posts.created_at desc"
		params[:cond] ||= "1=1"
		@posts = Post.parent_posts.includes(:children).where(params[:cond]).order(params[:sort]).page(params[:page]).per(params[:per]).references(:children)
	end

	def show
		@post = Post.find(params[:id])
		Post.update_all(["views = ?",@post.views += 1],["posts.id = ?",params[:id]]) if @post.present?
	end

	def create
		@post = Post.new(post_params)
		@post.user_id = current_user.id
		if @post.valid?
			@post.save
			@post.associate_subscribers(params[:subscribers]) if params[:subscribers] 	
			if params[:post][:categories].present?
				handle_api_error(@post) if !@post.associate_categories(params[:post][:categories])
			end
			Post.where(:id => @post.parent_post_id).first.touch if @post.parent_post_id.present?
			 
			render :show
		else
			handle_api_error(@post)
		end
	end

	def update
		@post = Post.unscoped.find(params[:id])
		if @post.update_attributes(post_params)  
			if params[:post][:categories].present?
				handle_api_error(@post) if !@post.associate_categories(params[:post][:categories])
			end
      @post = Post.find(params[:id])
			render :show
		else 
			handle_api_error(@post)
		end
	end


	def destroy
    @post = Post.unscoped.find(params[:id])
		@post.update_attribute("is_delete", true)
		render :json => {:result => "Post deleted"}
	end


	def kill
		raise "Cannot kill a post in prodction" if !Rails.env.development?
		@post = Post.unscoped.find(params[:post_id])
		@post.destroy if @post.present?
		render :json => {:result => "Post killed"}
	end

	def like
		@post = Post.find(params[:post_id])
		Like.create({:user_id => current_user.id , :object_type_id => @post.id,:object_type => "post"})
		render :show
	end

	##
	# Pulls a list of users that like a post
	def user_likes
		params[:page] ||= 1
		params[:per] ||= 25 
		likes  = Like.where(:object_type_id => params[:post_id],:object_type => "post").map{|u| u.user_id}
 		@users = User.where(:id => likes).page(params[:page]).per(params[:per])
		render :template => "/users/index"
	end

	def unlike
		@post = Post.find(params[:post_id])
		Like.where(:object_type_id => @post.id, :object_type => "post", :user_id => current_user.id).first.destroy
		render :json => {:result => "Like deleted"}
	end

	def favorite
		@post = Post.find(params[:post_id])
		Favorite.create({:user_id => current_user.id , :object_type_id => @post.id,:object_type => "post"})
		render :show
	end

	def unfavorite
		@post = Post.find(params[:post_id])
		Favorite.where(:object_type_id => @post.id, :object_type => "post", :user_id => current_user.id).first.destroy
		render :json => {:result => "Favorite deleted"}
	end

	def flag
		@post = Post.unscoped.find(params[:post_id])
		flag = (@post.flag.blank?) ? 1 : @post.flag += 1
		@post.update_attribute("flag",flag)
		@post.save
		render :json => {:result => "Post flagged"}
	end


	def categories
		@categories = Category.where(:object_type => "post").order(:name)
		render :json => {:post_categories => @categories.map{|c| c.name}   }
	end

	def destroy_category
		c = Category.where(:name => params[:id],:object_type => "post").first
		if c.present?
			c.destroy
			render :json => {:result => "Category deleted"}
		else
			handle_api_error("Category #{params[:id]} not found")
		end
	end

	private
	def post_params
		params.require(:post).permit(:parent_post_id,:post_title,:post_text,:public,:location,:zip,:user_id,:flag,:categories,assets_attributes:[:id,:file],post_subscribers_attributes:[:user_id,:group_id])
	end
end
 
