module SelectAttributesHelper

    def get_option_value(select_name, option_id)
   	  SelectAttribute.where(name: select_name).first.options[option_id.to_s]
 	end
end
