 show_auth ||= false
	 json.user do 
		 if user_instance.present?
		 json.categories user_instance.categories.map{|c| c.name }
		 	json.avatars do
		 		json.small  user_instance.avatar(:small)
		 		json.small2x  user_instance.avatar(:avatar_small2x)
		 		json.medium  user_instance.avatar(:avatar_medium)
		 		json.medium2x  user_instance.avatar(:avatar_medium2x)
		 		json.large  user_instance.avatar(:avatar_large)
		 		json.large2x  user_instance.avatar(:avatar_large2x)
		 		json.original  user_instance.avatar(:avatar_original)
		 	end
			json.roles do
				json.array!(user_instance.roles) do |u|
						json.name u.name
						json.id u.id
				end
			end
		 	json.is_invited user_instance.is_invited(@current_user.id)
		 	json.has_liked user_instance.has_liked(@current_user.id)
		 	json.meta_data user_instance.meta_data
		 	json.post_favorties_count user_instance.post_favorites.length
		 	json.(user_instance,:id,:user_type,:username,:email,:facebook_id,:twitter_id,:first_name,:last_name,:city,:state,:zip,:created_at,:description,:company_name,:is_business)
		 	json.device_id user_instance.device_id
		 	json.name user_instance.full_name
		 	json.posts_count user_instance.posts.count
		 	json.followers_count  user_instance.followers.count
		 	json.follows_count  user_instance.follows.count
		 	json.created time_ago_in_words(user_instance.created_at)
		 	json.is_following @current_user.present? ? @current_user.is_following(user_instance.id) : false
		 	if(show_auth)
		 		json.authentication_token user_instance.authentication_token
		 	end
		 end
	 end
	
 