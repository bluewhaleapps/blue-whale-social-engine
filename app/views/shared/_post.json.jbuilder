	json.(post, :id,:post_title,:post_text,:created_at,:updated_at,:parent_post_id,:views)
	json.created_timestamp  post.created_at.to_i
	json.updated_timestamp post.updated_at.to_i
	json.likes_count post.likes.count
	json.created time_ago_in_words(post.created_at)
	if @current_user.present?
		json.has_liked post.has_liked(@current_user.id) 
		json.is_favorite post.is_favorite(@current_user.id)
	end
	json.partial! 'shared/user', {:user_instance => post.user}
 	json.updated time_ago_in_words(post.updated_at)
 	json.comments_count post.comments.count
 	json.thread_count  post.thread_count

 	json.categories do 
 		if post.categories.present?
		 	json.array!(post.categories) do |c|
		 			json.name c.name 
		 	end
		else
		 	json.array!(post.categories) do |c|
		 		 
		 	end
		end
 	 end
 		
 		 

 
	 

	

