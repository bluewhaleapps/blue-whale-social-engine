 json.comment comment_instance.comment
 json.created time_ago_in_words(comment_instance.created_at)
 json.created_at comment_instance.created_at
 json.partial! 'shared/user', {:user_instance => comment_instance.user}