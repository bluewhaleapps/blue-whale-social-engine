class Comment < ActiveRecord::Base
	belongs_to :post,  :foreign_key => "object_type_id" 
	belongs_to :user

	validates_presence_of :comment,:user_id	

	default_scope { where("comments.deleted = false") }
end
