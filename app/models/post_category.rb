class PostCategory < ActiveRecord::Base
	belongs_to :category 
	belongs_to :post 
	validates_uniqueness_of :category ,:scope => :post
end
