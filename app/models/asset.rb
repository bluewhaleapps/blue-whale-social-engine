class Asset < ActiveRecord::Base

 belongs_to :post, :foreign_key => "object_type_id", :class_name => "Post"

  has_attached_file :file,   :storage => :s3,
   :s3_credentials => YAML.load_file('config/s3.yml'),
  :styles => lambda {|attachment|  attachment.instance.image_styles }, 
  :path => ":class/:attachment/:id/:basename-:style.:extension",
  :default_url => ":class/missing.png",
  :s3_permissions => "public-read"


  def image_styles
   if ["image/jpeg","image/jpg","image/png"].include?(file.content_type)
    {  
    :thumb2x => "200x200#",
    :thumb => "100x100#",
    :large => ["400x",:jpg],
    :large2x => ["800x",:jpg],
    }
  else  {}
   end
  end


  def is_photo?
    ["image/jpeg","image/jpg","image/png"].include?(self.file_content_type)
  end
end

