class Like < ActiveRecord::Base
	belongs_to :user
	validates :user_id, :uniqueness => {:scope => [:object_type_id,:object_type]}
end
