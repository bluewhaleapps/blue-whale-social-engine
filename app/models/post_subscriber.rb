class PostSubscriber < ActiveRecord::Base
	belongs_to :user
	belongs_to :group
	belongs_to :post
	 default_scope { where(:blocked => false) }
end
