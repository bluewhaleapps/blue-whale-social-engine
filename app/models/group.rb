class Group < ActiveRecord::Base
	belongs_to :user, class_name: 'User', foreign_key: "object_owner_id"
	has_many :user_groups, :dependent => :destroy
	has_many :users, :through => :user_groups
	has_many :post_subscribers, :dependent => :destroy
	validates :name, :presence => true
	validates :name, :uniqueness => true
	validates :users, :presence => true
	has_many :categories,  -> { where object_type: "group" } ,:foreign_key => "object_type_id"
	
	accepts_nested_attributes_for :users

end
