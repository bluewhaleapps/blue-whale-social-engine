class Invite < ActiveRecord::Base
	validates_uniqueness_of :invited_email, :scope => :user_id, :allow_blank => true
	validates_uniqueness_of :invited_id, :scope => :user_id, :allow_blank => true

	belongs_to :user
end
