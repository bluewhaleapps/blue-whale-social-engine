class Post < ActiveRecord::Base

	belongs_to :user 
	has_many :likes, -> { where object_type: "post" }, class_name: 'Like', foreign_key: "object_type_id"
	validates :user_id, :presence => true
	has_many :children, -> { joins(:user).where({:is_deleted => false})}, :class_name => "Post", :foreign_key => "parent_post_id", :dependent => :destroy
	has_many :assets,  -> { where object_type: "post" } ,:foreign_key => "object_type_id", :dependent => :destroy
	has_many :comments,  -> { where object_type: "post" } ,:foreign_key => "object_type_id", :dependent => :destroy
	has_many :post_categories, :dependent => :destroy
	has_many :categories, :through => :post_categories  
	has_many :post_subscribers, :dependent => :destroy
	has_many :favorites, -> { where object_type: "post" }, :foreign_key => "object_type_id", :dependent => :destroy
	default_scope { joins(:user).where(:users => {:is_deleted => false} ,:is_deleted => false) }

	accepts_nested_attributes_for :assets
	accepts_nested_attributes_for :post_subscribers
#	accepts_nested_attributes_for :categories

	after_create :set_asset_types

	scope :parent_posts , -> {where(:parent_post_id  => nil)}
	def has_liked(user_id)
		Like.exists?(["user_id = ? and object_type_id = ? and object_type = 'post'",user_id,self.id])
	end


	def thread_count
		if self.parent_post_id.present?
			Post.find(self.parent_post_id).children.count
		else 
			self.children.count
		end
	end

	def set_asset_types
		Asset.where(["object_type_id = ?",self.id]).update_all("object_type = 'post'")
	end


	def is_favorite(user_id)
		Favorite.exists?(["object_type_id = ? and object_type = 'post' and user_id = ?",self.id,user_id])
	end

	def subscribers
		subscribers = []
		_post = self
		_post = Post.where(:id => self.parent_post_id).first if self.parent_post_id.present?
		 
		_post.post_subscribers.each do |s|
			if s.user_id.present?
				subscribers << s.user
			else
				s.group.users.each do |g| 
					subscribers << g
				end
			end
		end
		 return subscribers
	end


 

	def associate_categories(items)
		self.post_categories.destroy_all
		items.each do |c|

			cat = Category.where(:name => c, :object_type => 'post').first
			if cat.present?
				self.post_categories.create({:category_id => cat.id})
			else 
				cat = Category.new({:name => c, :object_type => 'post'})
	 
					cat.save
					self.post_categories.create({:category_id => cat.id})
					return true
			 

			end
		end
		return true
	end

 

	def associate_subscribers(items)
		if items[:users].present?
			items[:users].each do |u|
				self.post_subscribers.create({:user_id => u})
			end
		end
		if items[:groups].present?
			items[:groups].each do |u|
				self.post_subscribers.create({:group_id => u})
			end
		end
	end

	
end
