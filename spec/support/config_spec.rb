class ConfigSpec

  def set_default_engine_config(api_key, config)
    config.tty = true
    config.color = true
    config.render_views = true

    config.include FactoryGirl::Syntax::Methods

    config.before(:suite) { DatabaseCleaner.clean_with(:truncation) }
    config.before(:each) { DatabaseCleaner.strategy = :transaction }
    #Macros
    config.include RegisterMacros

    config.before(:each) do
      DatabaseCleaner.start
      ConfigSpec.set_request_params api_key, request, register_user if defined? request
    end
    config.after(:each) { DatabaseCleaner.clean }

    FactoryGirl.register_strategy(:json, JsonStrategy)
  end

  def self.set_request_params api_key, request, register_user
    request.cookies['api_key'] = api_key
    request.cookies['authentication_token'] = register_user
    request.env["HTTP_ACCEPT"] = 'application/json'
  end
end