require 'spec_helper'

describe SessionsController do
  routes { BluewhaleSocial::Engine.routes }

  context 'Forgotten credentials' do
    let(:forgot_password_json) {{user: {email: 'test@test.com'}}}
    let(:forgot_password_with_case_json) { {user: {email: 'Test@test.com'}}}
    subject(:forgot_password_request) { put :forgot_password, forgot_password_json}
    subject(:forgot_password_cased_request) { put :forgot_password, forgot_password_with_case_json}
    subject(:forgot_username_request) { put :forgot_username, forgot_password_json}
    subject(:forgot_username_cased_request) { put :forgot_username, forgot_password_with_case_json}

    it { forgot_password_request.status.should be 200 }
    it { forgot_password_cased_request.status.should be 200 }
    it { forgot_username_request.status.should be 200 }
    it { forgot_username_cased_request.status.should be 200 }
  end

  context 'Facebook login when user was already created' do
    let(:facebook_user) { {user: json(:facebook_user_registered ) }}
    before(:each) { post :create, facebook_user }
    it { User.find(@user).oauth_token.should eq facebook_user[:user][:oauth_token] }
    it { User.find(@user).facebook_id.should_not eq facebook_user[:user][:oauth_token] }
  end

end