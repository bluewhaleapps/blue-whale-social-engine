require 'spec_helper'

describe UsersController do

  routes { BluewhaleSocial::Engine.routes }

  context 'User json Should have a role' do
 	
    let(:user_reqeust) { get :show, :id => @user.id} 	
      it 'be an array' do
    		JSON.parse(user_reqeust.body)['user']['roles'].class.should eq(Array)
      end
      it 'have a user_type' do
      	JSON.parse(user_reqeust.body)['user']['user_type'].should_not eq(nil)
      end
  end

  context 'User create with role' do

    it 'should add roles assignment to user' do
      put :update, id: @user.id, user: {roles_assignment: ['1']}
      subject.status.should be 200
    end
  end

  context 'User can save a role' do
      it  do
      	role = Role.new(:name => "Role 2")
     	  role.save
      	put(:update, :id => @user.id,:user => {:roles => [{:role_id => role.id}] })
      	JSON.parse(response.body)['user']['roles'].class.should eq(Array)
      end
  end

end