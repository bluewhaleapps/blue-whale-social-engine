class JsonStrategy
  def initialize
    @strategy = FactoryGirl.strategy_by_name(:build).new
  end

  delegate :association, to: :@strategy

  def result(evaluation)
    JSON.parse(@strategy.result(evaluation).to_json,symbolize_names: true)
  end
end