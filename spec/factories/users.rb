require 'faker'

FactoryGirl.define do

 factory :user do

    email Faker::Internet.free_email
    password_confirmation 'secret123'
    password 'secret123'
    username Faker::Internet.user_name('testName')

    factory :user_with_role_assignment do
      roles_assignment ['1']
    end

    factory :user_with_roles do
      after(:build) do |user|
        user.roles << build(:role)
      end
    end

     factory :registered_spec_user do
      email 'test@test.com'
      username 'test1234'

      factory :facebook_user_registered do
        facebook_id '11111'
        oauth_token 'test'
      end
    end

  end
end

 

